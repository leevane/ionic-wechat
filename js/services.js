angular.module('myApp.services', [])
.service("sidemenuleftService",function($http){
    return {
        getItems:function(){
            return $http.get("/data/sidemenu.json");
        }
    }
})
.service("messageServices",function($http){
    return {
        getItems:function(){
            return $http.get("/data/weixinitems.json");
        }
    }
})
.service("popoverServices",function($http){
    return {
        getItems:function(){
            return $http.get("/data/popoverItems.json");
        }
    }
})