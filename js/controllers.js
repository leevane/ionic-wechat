angular.module('myApp.controllers', [])
    
    .constant('$ionicLoadingConfig',{
	    content: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0,
        duration:1000
    })
    .controller('sidemenuController',function($scope,sidemenuleftService){
        console.log("sidemenuController");
        sidemenuleftService.getItems().success(function(result){
            $scope.items=result;
        });
    })
    .controller('messageController', function($scope, $state, $http, $ionicPopover, $ionicLoading, messageServices, popoverServices) {
        console.log("messageController");
        $scope.title = '微信';
        $scope.items=[];
        $ionicLoading.show();
        messageServices.getItems().success(function(result){
            for(var i=0,len=result.length;i<len;i++){
                result[i].title += ""+i;
                $scope.items.push(result[i]);
            }
        })
        /*左滑动显示通讯录页面*/
        $scope.onSwipeLeft = function() {
            $state.go("tab.friends");
        };

        /*点击顶部加号 显示popover*/
        $scope.popoverItems = [];
        popoverServices.getItems().success(function(result){
            for(var i=0,len=result.length;i<len;i++){
                $scope.popoverItems.push(result[i]);
            }
        })

        $ionicPopover.fromTemplateUrl('my-popover.html', {
            scope: $scope,
            cssClass:"mypopover"
        }).then(function(popover) {
            $scope.popover = popover;
        });
        $scope.openPopover = function($event) {
            $scope.popover.show($event);
        };

        /*下拉刷新*/
        $scope.doRefresh=function(){
            messageServices.getItems().success(function(result){
                for(var i=0,len=result.length;i<len;i++){
                    result[i].title += ""+i;
                    $scope.items.push(result[i]);
                }
            })
            .finally(function() {
                $scope.$broadcast('scroll.refreshComplete');
            });
        }
        /*当用户滚动的距离超出底部的内容时，就执行on-infinite
         *如果不满一屏数据也会执行这里至到满足一屏的数据 
         */
        $scope.loadMore=function(){
            messageServices.getItems().success(function(result){
                for(var i=0,len=result.length;i<len;i++){
                    result[i].title += ""+i;
                    $scope.items.push(result[i]);
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
            })
            .finally(function() {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }

        /*微信页面 搜索功能*/
        $scope.searchMessageTitle="";
    })
    .controller('messageDetailController', function($scope, $state, $stateParams) {
        console.log("messageDetailController",$stateParams.id);
        $scope.id=$stateParams.id;
        $scope.onSwipeLeft = function() {
            $state.go("tab.friends");
        };
        $scope.onSwipeRight = function() {
            $state.go("tab.message");
        };
    })
    .controller('friendsController', function($scope, $state) {
        console.log("friendsController");
        $scope.title = '通讯录';
        
        $scope.onSwipeLeft = function() {
            $state.go("tab.discovery");
        };
        $scope.onSwipeRight = function() {
            $state.go("tab.message");
        };
    })
    .controller('discoveryController', function($scope, $state) {
        console.log("discoveryController");
        $scope.title = '发现';
        $scope.onSwipeLeft = function() {
            $state.go("tab.my");
        };
        $scope.onSwipeRight = function() {
            $state.go("tab.friends");
        };
    })
    .controller('myController', function($scope, $state) {
        console.log("myController");
        $scope.title = '我';
        $scope.onSwipeRight = function() {
            $state.go("tab.discovery");
        };
    });