angular.module('myApp', ['ionic', 'myApp.controllers', 'myApp.services'])

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('tab', {
            url: "/tab",
            abstract: true,
            templateUrl: "templates/tabs.html"
        })
        .state('tab.message', {
            url: '/message',
            views: {
                'tab-message': {
                    templateUrl: "templates/message.html",
                    controller: 'messageController'
                }
            }
        })
        .state('tab.message-detail', {
            url: '/message/:id',
            views: {
                'tab-message': {
                    templateUrl: 'templates/message-detail.html',
                    controller: 'messageDetailController'
                }
            }
        })
        .state('tab.friends', {
            url: '/friends',
            views: {
                'tab-friends': {
                    templateUrl: "templates/friends.html",
                    controller: 'friendsController'
                }
            }
        })
        .state('tab.discovery', {
            url: '/discovery',
            views: {
                'tab-discovery': {
                    templateUrl: "templates/discovery.html",
                    controller: 'discoveryController'
                }
            }
        })
        .state('tab.my', {
            url: '/my',
            views: {
                'tab-my': {
                    templateUrl: "templates/my.html",
                    controller: 'myController'
                }
            }
        });
    /*.state('index.content1', {
        url: '/content1/:id',
        views: {
            'tab-tab1': {
                templateUrl: "templates/tab-content1.html",
                controller: 'content1Controller'
            }
        }
    })
    .state('news', {
        url: '/news',
        templateUrl: "templates/news.html"
    })*/
    $urlRouterProvider.otherwise('/tab/message');
});